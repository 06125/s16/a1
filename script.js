/* //Solution 1: Using DOM
function getInfo() {
	const firstName = document.getElementById("firstName").value;
	const lastName = document.getElementById("lastName").value;
	const email = document.getElementById("email").value;
	const password = document.getElementById("password").value;
	const confirmPassword = document.getElementById("confirmPassword").value;

	if (!firstName || !lastName || !email || !password || !confirmPassword) {
		alert("Please fill in your information.");
	} else if (password.length < 8) {
		alert("Password should be at least 8 characters.");
	} else if (password !== confirmPassword) {
		alert("Please double check your password.");
	} else {
		alert("Thanks for logging your information");
	}
}
*/

//Solution 2
function getInfo(firstName, lastName, email, password, confirmPassword) {
	if (!firstName || !lastName || !email || !password || !confirmPassword) {
		console.log("Please fill in your information.");
	} else if (password.length < 8) {
		console.log("Password should be at least 8 characters.");
	} else if (password !== confirmPassword) {
		console.log("Please double check your password.");
	} else {
		console.log("Thanks for logging your information");
	}
}